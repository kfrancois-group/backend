import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'auth/auth.module';
import { RouterModule } from 'nest-router';
import { ROUTES } from 'routes';
import { UserModule } from 'user/user.module';
import { RecipeModule } from './recipe/recipe.module';

const MODULES_IMPORT = [
  RouterModule.forRoutes(ROUTES),
  TypeOrmModule.forRoot(),
];

const MODULES_USER = [AuthModule, RecipeModule, UserModule];

@Module({
  imports: [...MODULES_IMPORT, ...MODULES_USER],
})
export class AppModule {}
