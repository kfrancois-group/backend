import { Test, TestingModule } from '@nestjs/testing';
import { RecipeController } from './recipe.controller';

describe('AppController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [RecipeController],
    }).compile();
  });

  describe('root', () => {
    it('should return "Hello World!"', () => {
      const recipeController = app.get<RecipeController>(RecipeController);
      expect(recipeController.findAll()).toBe('test');
    });
  });
});
