import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthModule } from 'auth/auth.module';
import { Ingredient } from './entities/ingredient.entity';
import { Recipe } from './entities/recipe.entity';
import { RecipeController } from './recipe.controller';
import { RecipeService } from './services/recipe.service';

@Module({
  controllers: [RecipeController],
  imports: [TypeOrmModule.forFeature([Recipe, Ingredient]), AuthModule],
  providers: [RecipeService],
})
export class RecipeModule {}
