import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityService } from 'common/services/entity.service';
import { CreateRecipeDto } from 'recipe/dto/create-recipe.dto';
import { Recipe } from 'recipe/entities/recipe.entity';
import { Repository } from 'typeorm';

@Injectable()
export class RecipeService extends EntityService<Recipe> {
  constructor(@InjectRepository(Recipe) recipeRepository: Repository<Recipe>) {
    super(recipeRepository);
  }

  async createRecipe(recipe: CreateRecipeDto): Promise<Recipe> {
    return await super.create(recipe);
  }
}
