import { Type } from 'class-transformer';
import {
  ArrayMinSize,
  IsArray,
  IsString,
  MinLength,
  ValidateNested,
} from 'class-validator';
import { CreateIngredientDto } from './create-ingredient.dto';

export class CreateRecipeDto {
  @IsString()
  @MinLength(3)
  name: string;

  @IsArray()
  @ArrayMinSize(2, { message: 'recipe must contain at least two ingredients' })
  @ValidateNested({ always: true })
  @Type(type => CreateIngredientDto)
  ingredients: CreateIngredientDto[];
}
