import {
  Body,
  Controller,
  Get,
  Post,
  UseGuards,
  UsePipes,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiUseTags } from '@nestjs/swagger';
import { EntityController } from 'common/controllers/entity.controller';
import { CreateRecipeDto } from './dto/create-recipe.dto';
import { Recipe } from './entities/recipe.entity';
import { ValidationPipe } from './pipes/validation.pipe';
import { RecipeService } from './services/recipe.service';

@Controller()
@ApiUseTags('Recipes')
@UsePipes(new ValidationPipe())
@UseGuards(AuthGuard('jwt'))
export class RecipeController extends EntityController<Recipe> {
  constructor(private readonly recipeService: RecipeService) {
    super(recipeService);
  }

  @Get()
  async findAll() {
    return super.findAll();
  }

  @Post()
  async create(@Body() createRecipeDto: CreateRecipeDto): Promise<Recipe> {
    return await this.recipeService.create(createRecipeDto);
  }
}
