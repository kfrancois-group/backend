import { Column, Entity, OneToMany, PrimaryGeneratedColumn } from 'typeorm';
import { Ingredient } from './ingredient.entity';

@Entity()
export class Recipe {
  @PrimaryGeneratedColumn()
  id: number;

  @Column({ length: 255, nullable: false })
  name: string;

  @OneToMany(() => Ingredient, ingredient => ingredient.recipe, {
    cascade: true,
  })
  ingredients: Ingredient[];
}
