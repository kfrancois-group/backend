import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm';
import { Recipe } from './recipe.entity';

@Entity()
export class Ingredient {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Recipe, recipe => recipe.ingredients, {
    onDelete: 'CASCADE',
  })
  recipe: Recipe;

  @Column({ length: 255 })
  name: string;
}
