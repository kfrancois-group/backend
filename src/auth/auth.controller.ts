import {
  Body,
  Controller,
  Get,
  HttpException,
  HttpStatus,
  Post,
  UseGuards,
} from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiImplicitHeader, ApiResponse, ApiUseTags } from '@nestjs/swagger';
import { LoginUserDto } from '../user/dto/login-user.dto';
import { UserService } from '../user/services/user.service';
import { AuthService } from './auth.service';

interface Token {}

@Controller()
@ApiUseTags('Authentication')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UserService,
  ) {}

  @Post('token')
  async createToken(@Body() loginUserDto: LoginUserDto): Promise<Token> {
    const user = await this.userService.validate(loginUserDto);

    return this.authService.createToken(user);
  }

  @UseGuards(AuthGuard('jwt'))
  @Get('token_check')
  @ApiImplicitHeader({ name: 'authorization', description: 'Token' })
  @ApiResponse({ status: 200, description: 'Valid token' })
  @ApiResponse({ status: 401, description: 'Invalid token' })
  async tokenCheck(): Promise<Token> {
    return;
  }
}
