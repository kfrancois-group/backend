import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { LoginUserDto } from '../user/dto/login-user.dto';
import { JwtPayload } from './interfaces/jwt-payload.interface';

@Injectable()
export class AuthService {
  constructor(private readonly jwtService: JwtService) {}

  async createToken(user: LoginUserDto): Promise<JwtPayload> {
    const { emailAddress } = user;

    const token = this.jwtService.sign({ emailAddress });

    return {
      accessToken: token,
      ...(this.jwtService.decode(token, { json: true }) as any), // TODO
    };
  }
}
