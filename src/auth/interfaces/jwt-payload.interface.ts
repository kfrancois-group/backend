export interface JwtPayload {
  emailAddress: string;
  iat: number;
  exp: number;
}
