import { AuthModule } from 'auth/auth.module';
import { RecipeModule } from 'recipe/recipe.module';
import { UserModule } from 'user/user.module';

export const ROUTES = [
  {
    path: 'api',
    children: [
      { path: '/recipe', module: RecipeModule },
      { path: '/auth', module: AuthModule },
      { path: '/user', module: UserModule },
    ],
  },
];
