import { Body, Controller, Post, UsePipes } from '@nestjs/common';
import { ApiUseTags } from '@nestjs/swagger';
import { EntityController } from 'common/controllers/entity.controller';
import { ValidationPipe } from 'recipe/pipes/validation.pipe';
import { CreateUserDto } from 'user/dto/create-user.dto';
import { User } from 'user/entities/user.entity';
import { UserService } from 'user/services/user.service';

@Controller()
@ApiUseTags('User')
@UsePipes(new ValidationPipe())
export class UserController extends EntityController<User> {
  constructor(private readonly userService: UserService) {
    super(userService);
  }

  @Post('register')
  async create(@Body() createUserDto: CreateUserDto): Promise<User> {
    return await this.userService.create(createUserDto);
  }
}
