import { IsEmail, IsString, MinLength } from 'class-validator';

export class CreateUserDto {
  @IsEmail()
  emailAddress: string;

  @IsString()
  @MinLength(12)
  password: string;
}
