import { ApiModelProperty } from '@nestjs/swagger';
import { IsEmail, IsString, MinLength } from 'class-validator';

export class LoginUserDto {
  @ApiModelProperty({ required: true, example: 'email@address.com' })
  @IsEmail()
  emailAddress: string;

  @ApiModelProperty({
    required: true,
    minLength: 12,
    example: 'MySecretPassword',
  })
  @IsString()
  @MinLength(12)
  password: string;
}
