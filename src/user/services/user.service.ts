import { BadRequestException, Injectable, Post } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityService } from 'common/services/entity.service';
import { Repository } from 'typeorm';
import { CreateUserDto } from '../../user/dto/create-user.dto';
import { LoginUserDto } from '../../user/dto/login-user.dto';
import { User } from '../../user/entities/user.entity';

@Injectable()
export class UserService extends EntityService<User> {
  constructor(
    @InjectRepository(User) private readonly userRepository: Repository<User>,
  ) {
    super(userRepository);
  }

  @Post()
  async create(user: CreateUserDto): Promise<User> {
    return await super.create(user);
  }

  async validate(loginUserDto: LoginUserDto): Promise<User> {
    const { emailAddress, password } = loginUserDto;

    const user = await this.userRepository.findOne({
      where: { emailAddress, password },
    });

    if (!user) {
      throw new BadRequestException('invalid email address and/or password');
    }

    return user;
  }

  async findByEmailAddress(emailAddress: string): Promise<User> {
    return await this.userRepository.findOne({ where: { emailAddress } });
  }
}
