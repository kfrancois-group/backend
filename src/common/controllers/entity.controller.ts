import {
  Body,
  Delete,
  Get,
  Injectable,
  Param,
  Post,
  Put,
} from '@nestjs/common';
import { IEntityController } from 'common/interfaces/entity-controller.interface';
import { IEntity } from 'common/interfaces/entity.interface';
import { EntityService } from 'common/services/entity.service';
import { DeepPartial } from 'typeorm';

@Injectable()
export class EntityController<T extends IEntity>
  implements IEntityController<T> {
  constructor(private readonly entityService: EntityService<T>) {}

  @Get()
  async findAll(): Promise<T[]> {
    return await this.entityService.findAll();
  }

  @Get(':id')
  async find(@Param('id') id: number): Promise<T> {
    return await this.entityService.find(id);
  }

  @Post()
  async create(@Body() entity: DeepPartial<T>): Promise<T> {
    return await this.entityService.create(entity);
  }

  @Put()
  async update(@Body() entity: DeepPartial<T>): Promise<T> {
    return await this.entityService.update(entity);
  }

  @Delete(':id')
  async delete(@Param('id') id: number) {
    return await this.entityService.delete(id);
  }
}
