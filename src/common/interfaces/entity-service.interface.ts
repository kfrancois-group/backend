import { DeepPartial } from 'typeorm';

export interface IEntityService<T> {
  findAll(): Promise<T[]>;
  find(id: number): Promise<T>;
  create(entity: DeepPartial<T>): Promise<T>;
  update(entity: DeepPartial<T>): Promise<T>;
  delete(id: number);
}
