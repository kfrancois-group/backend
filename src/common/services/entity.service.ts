import {
  BadRequestException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { IEntityService } from 'common/interfaces/entity-service.interface';
import { IEntity } from 'common/interfaces/entity.interface';
import { DeepPartial, Repository } from 'typeorm';
import { isNumber } from 'util';

@Injectable()
export class EntityService<T extends IEntity> implements IEntityService<T> {
  constructor(private readonly entityRepository: Repository<T>) {}

  async findAll(): Promise<T[]> {
    return await this.entityRepository.find();
  }
  async find(id: number): Promise<T> {
    const entity = await this.entityRepository.findOne(id);

    if (entity === undefined) {
      throw new NotFoundException();
    }
    return entity;
  }
  async create(entity: DeepPartial<T>): Promise<T> {
    // Type assertion as any necessary since Typescript can't assert
    return await this.entityRepository.save(entity as any);
  }
  async update(entity: DeepPartial<T>): Promise<T> {
    if (!isNumber(entity.id)) {
      throw new BadRequestException(
        `'id' should be a number, received '${entity.id}'`,
      );
    }
    await this.find(entity.id);

    return await this.entityRepository.save(entity as any);
  }
  async delete(id: number) {
    await this.find(id);
    return await this.entityRepository.delete(id);
  }
}
