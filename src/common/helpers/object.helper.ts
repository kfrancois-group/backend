export const objectToValues = (object: object) => {
  return Object.keys(object).map(key => object[key]);
};

export const flattenObject = object => {
  const nestElement = (prev, value, key) => {
    return value && typeof value === 'object'
      ? { ...prev, ...flattenObject(value) }
      : { ...prev, ...{ [key]: value } };
  };

  return Object.keys(object).reduce(
    (prev, key) => nestElement(prev, object[key], key),
    {},
  );
};

export const mapByKey = (object, searchKey: string) => {
  let value;
  Object.keys(object).some(key => {
    if (key === searchKey) {
      value = object[key];
      return true;
    }
    if (object[key] && typeof object[key] === 'object') {
      value = mapByKey(object[key], searchKey);
      return value !== undefined;
    }
  });
  return value;
};
