export const isNumber = (value: any): boolean => {
  return value && !isNaN(Number(value.toString()));
};
