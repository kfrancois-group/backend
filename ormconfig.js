module.exports = {
  type: 'mysql',
  host: 'database',
  port: 3306,
  synchronize: true,
  username: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
  entities: ['src/**/**.entity.ts'],
  migrationsTablename: 'migrations',
  migrations: ['src/migrations/*.migration.ts'],
  cli: {
    migrationsDir: 'src/migrations',
  },
};
